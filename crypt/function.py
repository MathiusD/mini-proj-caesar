def generate_alphabet(mini=' ', maxi='~'):
    alphabet = []
    for char in range(ord(mini), ord(maxi) + 1):
        alphabet.append(chr(char))
    return alphabet

def generate_alphabet_A_Z(upper=False):
    if upper == True:
        return generate_alphabet('A', 'Z')
    else:
        return generate_alphabet('a', 'z')

def in_alphabet(char, A_Z=False, upper=False):
    if A_Z == False:
        if char in generate_alphabet():
            return True
        else:
            return False
    else:
        if char in generate_alphabet_A_Z(upper):
            return True
        else:
            return False

def decalage(char, maxi=ord('~'), mini=ord(' ')):
    output = char
    while in_alphabet(output) == False:
        code = ord(output)
        if code > maxi:
            code = code % maxi
            code = code + mini
        elif code < mini:
            temp = code - mini
            code = maxi - temp
        output = chr(code)
    return output
        

def cryptage(chaine, cle, cycl=0):
    output = ""
    for i in range(len(chaine)):
        if cycl == 0:
            char = chr(ord(chaine[i])+cle)
        else:
            if cycl == 1:
                if in_alphabet(chaine[i]) == True:
                    char = decalage(chr(ord(chaine[i])+cle))
                else:
                    char = chaine[i]
            else:
                if in_alphabet(chaine[i], True, True) == True:
                    char = decalage(chr(ord(chaine[i])+cle), ord('Z'), ord('A'))
                elif in_alphabet(chaine[i], True) == True:
                    char = decalage(chr(ord(chaine[i])+cle), ord('z'), ord('a'))
                else:
                    char = chaine[i]
        output = output + char
    return output

def traduction(chaine, cle, cycl=0):
    output = ""
    for i in range(len(chaine)):
        if cycl == 0:
            char = chr(ord(chaine[i])-cle)
        else:
            if cycl == 1:
                if in_alphabet(chaine[i]) == True:
                    char = decalage(chr(ord(chaine[i])+cle))
                else:
                    char = chaine[i]
            else:
                if in_alphabet(chaine[i], True, True) == True:
                    char = decalage(chr(ord(chaine[i])-cle), ord('Z'), ord('A'))
                elif in_alphabet(chaine[i], True) == True:
                    char = decalage(chr(ord(chaine[i])-cle), ord('z'), ord('a'))
                else:
                    char = chaine[i]
        output = output + char
    return output