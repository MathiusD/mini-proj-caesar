from tkinter.messagebox import *
from tkinter import *
from tkinter import ttk
from crypt.function import cryptage, traduction

def cyclique_ou_non():
    if cyclique.get() == 'Non-cyclique':
        return 0
    elif cyclique.get() == 'CycliqueFull':
        return 1
    else:
        return 2

def button_trad_callback():
    precrypt.delete("0.0", END)
    precrypt.insert("0.0", traduction(postcrypt.get("0.0", END).strip(), cle.get(), cyclique_ou_non()))

def button_crypt_callback():
    postcrypt.delete("0.0", END)
    postcrypt.insert("0.0", cryptage(precrypt.get("0.0", END).strip(), cle.get(), cyclique_ou_non()))

root = Tk()
root.title("Crypt")

cle = IntVar()
cle.set(8)
entry = Entry(root, textvariable=cle, width=30)
entry.grid(column=0, row=0)

cyclique = ttk.Combobox(root, values=['Non-cyclique','CycliqueFull', 'Cyclique A-Z'])
cyclique.current(0)
cyclique.grid(column=1, row=0)

precrypt = Text(root, wrap='word') # création du composant
precrypt.insert(END, "") # insertion d'un texte
precrypt.grid(column=0, row=1)

postcrypt = Text(root, wrap='word') # création du composant
postcrypt.insert(END, "") # insertion d'un texte
postcrypt.grid(column=1, row=1)

root.rowconfigure(0, weight=1)
root.columnconfigure(1, weight=1)

button_trad = Button(root, text="Traduire !", command=button_trad_callback)
button_trad.grid(row=2, column=1)

button_crypt = Button(root, text="Crypter !", command=button_crypt_callback)
button_crypt.grid(row=2, column=0)

root.mainloop()