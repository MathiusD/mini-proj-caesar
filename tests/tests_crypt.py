import unittest
from crypt.function import cryptage, traduction

class testCrypt(unittest.TestCase):
    
    def test_cryptage(self):
        self.assertEqual('JKL', cryptage('ABC', 9))
        self.assertEqual(')', cryptage('~', 9, 1))
        self.assertEqual('~', cryptage('~', 9, 2))

    def test_traduction(self):
        self.assertEqual('ABC', traduction('KLM', 10))
        self.assertEqual(' ', traduction('*', 10, 1))
        self.assertEqual(' ', traduction(' ', 10, 2))

    def test_croise(self):
        self.assertEqual('WXZ', cryptage(traduction('WXZ', 9), 9))
        self.assertEqual('WXZz', cryptage(traduction('WXZz', 9, 1), 9, 1))
        self.assertEqual('WX Z', cryptage(traduction('WX Z', 9, 2), 9, 2))